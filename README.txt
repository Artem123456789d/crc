Контрольная сумма -- некоторое число, которое передается вместе с сообщением
для проверки целостности данных при их передаче или хранении.

Основная идея алгоритма CRC состоит в представлении исходного сообщения в виде
двоичного числа, делении его на другое фиксированное число(полином-генератор).
Полученный при данном делении остаток и будет являться контрольной суммой.

CRC-АРИФМЕТИКА
Для вычисления контрольной суммы с помощью алгоритма CRC используется арифметика
по модулю 2 без переносов бит. Например:
 10011101     10011101
+11010010    -11010010
---------    ---------
 01001101     01001101

И сложение, и вычитание в CRC арифметике идентичны операции <<исключающего или>>(XOR).
Для операции деления необходимо определить операцию сравнения двух чисел.
Число А больше или равно числу Б, если количество бит без учета незначащих нулей числа А 
больше или равна количеству бит без учета незначащих нулей числа Б.
В остальном, операция деления идентична привычному делению "в столбик"

Для вычисления контрольной суммы с использованием алгоритма CRC8 (8 означает позицию 
старшего единичного бита в полиноме) обычно используется
полином-генератор 11010101 (X^7+X^6+X^4+X^2+X^0)

АЛГОРИТМ РЕШЕНИЯ
1) Файл считывается в массив байт
2) Массив байт преобразовывается в бинарную строку(Набор бит)
3) Берется очередной бит из бинарной строки и записывается в промежуточный результат
	контрольной суммы. 
4) Если промежуточный результат контрольной суммы больше 
	или равен полиному-генератору, проводится опеация XOR, результат
	записывается как промежуточный результат контрольной суммы.
5) Если все биты бинарной строки не обработаны, вернуться к шагу 3.

Например:
Исходное сообщение: 111010001011010
Полином-генератор:  11010101

1. Остаток	     - 1
2. Остаток	     - 11
3. Остаток	     - 111
4. Остаток	     - 1110
5. Остаток	     - 11101
6. Остаток	     - 111010
7. Остаток	     - 1110100
8. Остаток	     - 11101000
   Полином-генератор - 11010101
(Операция XOR)         00111101
   Остаток	     - 111101
9. Остаток	     - 1111011
10.Остаток	     - 11110110
   Полином-генератор - 11010101
(Операция XOR)         00100011
   Остаток	     - 100011
11.Остаток	     - 1000111
12.Остаток	     - 10001111
   Полином-генератор - 11010101
(Операция XOR)         01011010
   Остаток	     - 1011010
13.Остаток	     - 10110100
   Полином-генератор - 11010101
(Операция XOR)         01100001
   Остаток	     - 1100001
14.Остаток	     - 11000011
   Полином-генератор - 11010101
(Операция XOR)         00010110
   Остаток	     - 10110
15.Остаток	     - 101100
16. Все биты исходного сообщения обработаны. Контрольная сумма - 44

Использованный источник:
Ross N. Williams. A PAINLESS GUIDE TO CRC ERROR DETECTION ALGORITHMS(1993)

(Рабочей папкой считать папку, в которой находится файл README.txt,
Для запуска приложения необходима Java версии 11 или новее)
Запуск приложения:
Открыть проект с помощью Intellij IDEA
Запустить метод main в файле src/main/java/CRC.java

Если открытие проекта с помощью IDEA невозможно,
в консоли из рабочей папки проекта выполнить:
java -jar CRC8-1.0.jar

Исходный код программы находится в файле src/main/java/CRC.java