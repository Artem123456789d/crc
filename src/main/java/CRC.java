import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CRC {
    private static final String POLYNOMIAL = "11010101";


    private static List<Byte> readFile(String path) throws IOException {
        //Чтение байт файла
        File file = new File(path);
        if (!file.exists()) {
            throw new FileNotFoundException("File not found.");
        }
        FileInputStream fileInputStream = new FileInputStream(file);

        List<Byte> result = new ArrayList<>();

        for (byte b : fileInputStream.readAllBytes()) {
            result.add(b);
        }

        return result;
    }

    private static char XOR(char a, char b) {
        if (a == b) {
            return '0';
        } else {
            return '1';
        }
    }

    private static String XOR(String a, String b) {
        String res = "";
        if (a.length() != b.length()) {
            throw new IllegalStateException();
        }

        for (int i = 0; i < a.length(); i++) {
            res += XOR(a.charAt(i), b.charAt(i));
        }
        return res;
    }

    private static int calculateCRC(List<Byte> bytes) {
        //Вычисление контрольной суммы
        /*
Массив байт преобразовывается в бинарную строку(Набор бит)
Берется очередной бит из бинарной строки и записывается в промежуточный результат
	контрольной суммы.
Если промежуточный результат контрольной суммы больше
	или равен полиному-генератору, проводится опеация XOR, результат
	записывается как промежуточный результат контрольной суммы.
         */
        String binaryString = "";
        if (bytes.isEmpty()) {
            return 0;
        }
        binaryString += Integer.toBinaryString(bytes.get(0));
        bytes.remove(0);
        while (binaryString.length() < 8) {
            binaryString = '0' + binaryString;
        }
        String buf = "";
        while (!binaryString.isEmpty() || buf.length() >= POLYNOMIAL.length()) {
            if (buf.length() < POLYNOMIAL.length()) {
                buf += binaryString.charAt(0);
                binaryString = binaryString.substring(1);
                if (binaryString.length() <= 1 && !bytes.isEmpty()) {
                    String helper = Integer.toBinaryString(bytes.get(0));
                    bytes.remove(0);
                    while (helper.length() < 8) {
                        helper = '0' + helper;
                    }
                    binaryString += helper;
                }
            } else {
                buf = XOR(buf, POLYNOMIAL);
                buf = Integer.toBinaryString(Integer.parseInt(buf, 2));
            }
        }
        return Integer.parseInt(buf, 2);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Write absolute path to the file: ");
        //Чтение пути к файлу с консоли
        String filePath = scanner.nextLine();
        List<Byte> bytes;
        try {
            //получение байтов файла
            bytes = readFile(filePath);
        } catch (FileNotFoundException e) {
            //файл не найден
            System.out.println("File not found.");
            return;
        } catch (IOException e) {
            //Ошибка чтения файла
            System.out.println("Error while reading file.");
            return;
        }
        //Результат -- контрольная сумма
        int result = calculateCRC(bytes);
        System.out.println("Checksum: " + result);
    }
}
